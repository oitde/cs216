#!/bin/bash

mypath=`realpath $0`
mybase=`dirname $mypath`
# This causes the script to return an error if the file is missing
#source $mybase/utils.bashrc

if [ ! -e ~/.bash_aliases ]; then
    ln -s $mybase/bashrc_aliases ~/.bash_aliases
fi

######################################################################
if ! sudo grep -q 'US/Eastern' /etc/timezone; then
    echo 'US/Eastern' | sudo tee /etc/timezone
    sudo dpkg-reconfigure --frontend noninteractive tzdata
fi

######################################################################
# basics:
sudo apt-get -qq update
sudo apt-get -q --yes autoremove
sudo apt-get -q --yes dist-upgrade
sudo apt-get -q --yes install wget nano vim man

######################################################################
# lubuntu-desktop:
sudo apt-get -qq --yes install --no-install-recommends lubuntu-desktop

######################################################################
# disable lubuntu screen lock; was causing problems on VMs:
if [ -e /etc/xdg/autostart/light-locker.desktop ]; then
    sudo mv /etc/xdg/autostart/light-locker.desktop /etc/xdg/autostart/light-locker.desktop.bak
fi

######################################################################
# python:
sudo apt-get -q --yes install python python-pip python-dev
sudo pip install "ipython[all]"
sudo apt-get -q --yes install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose
sudo apt-get -q --yes install python-sklearn


######################################################################
# java:
sudo apt-get -q --yes install default-jre default-jdk

######################################################################
# postgresql:
sudo apt-get -q --yes install postgresql postgresql-contrib libpq-dev libpg-java
sudo pip install psycopg2 sqlalchemy
if ! sudo -u postgres psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='$user'" | grep -q 1; then
    sudo -u postgres createuser -s $user
    sudo -u postgres psql -c "ALTER USER $user WITH PASSWORD '$dbpasswd'"
fi
postgresql_conf=/etc/postgresql/9.3/main/postgresql.conf
if ! sudo grep -q 'BEGIN DATACOURSE CUSTOMIZATION' $postgresql_conf; then
    sudo tee -a $postgresql_conf << EOF
### BEGIN DATACOURSE CUSTOMIZATION
# logs by default will be found in /var/lib/postgresql/9.3/main/pg_log/
logging_collector = on
log_statement = 'all'
### END DATACOURSE CUSTOMIZATION
EOF
fi
sudo /etc/init.d/postgresql restart

######################################################################
# xml:

sudo apt-get -q --yes install libxml2-dev libxslt1-dev libxml2-utils
sudo apt-get -q --yes install libsaxonb-java
sudo pip install lxml

######################################################################
# chrome:
if ! sudo apt-key list | grep -q 'Google'; then
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
    sudo tee -a /etc/apt/sources.list.d/google.list <<EOF
deb http://dl.google.com/linux/chrome/deb/ stable main
EOF
    sudo apt-get -qq update
fi
sudo apt-get -q --yes install google-chrome-stable

#############
# install and config xrdp
#############
sudo apt-get install -y xrdp

sudo sed -i.bak -e 's?. /etc/X11/Xsession?#. /etc/X11/Xsession\nlxsession -e LXDE -s Lubuntu?' /etc/xrdp/startwm.sh

# Remove x11 call 
sudo sed -i -e 's?. /etc/X11/Xsession?#. /etc/X11/Xsession?' /etc/xrdp/startwm.sh
echo 'lxsession -e LXDE -s Lubuntu' >> /etc/xrdp/startwm.sh




