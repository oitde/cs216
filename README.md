## Synopsis

This code is used to deploy an Application Stack on an Ubuntu16 VM to set up a VM for use in the
'COMPSCI 216 - Everything Data' course. Python, Postgres, etc.  It should be run from the host on which you want it deployed, Using an ansible playbook


## Usage

```
./install.sh
```

## Setting the users' window manager 

We need to place a .xsession file in each users' home directory to tell X   
which window manager to run. The content of .xsession should be something like this

```
     lxsession -e LXDE -s Lubuntu
```

and it may take a restart of the xrdp service for it to notice the .xsession setting.  
You can restart xrtdp like this:

```
     sudo service xrdp restart
```

